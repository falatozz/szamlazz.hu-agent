# Szamlazz.hu Agent kliens #

Fejlesztő: Tibor Csik <csulok0000@gmail.com, t.csik@falatozz.hu>

A szamlazz.hu által működtetett Agent api használatához készült PHP könyvtár mely jelenleg a következő funkciókat támogatja:
* Agent::createBill - Számla létrehozása
* Agent::storno - Számla sztornózása