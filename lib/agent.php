<?php

/**
 * 
 * Számlázz.hu Agent api kliens
 * 
 * Minimum PHP 5.4 szükséges hozzá
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

include_once __DIR__ . '/agentresponse.php';
include_once __DIR__ . '/error.php';
include_once __DIR__ . '/responsedata.php';

/**
 * 
 * 
 */
class Agent {
    
    /**
     * Az Agent Api elérése
     */
    const HOST = 'https://www.szamlazz.hu/szamla/';
    
    /**
     * 
     * A kommunikáció során használt sütik tárolására
     *
     * @var string
     */
    public $cookieFile = '';
    
    /**
     *
     * @var \Logger
     */
    public $logger = null;
    
    /**
     * 
     * @param string $xmlFile
     * @return AgentResponse
     */
    public function createBill($xmlFile) {
        $data = [];
        
        /**
         * PHP 5.5-től létezik a CURFile, előtte csak egy speciálisan összeálíltott stringet kellet átadni ha fájlt akarunk csatolni a formhoz
         */
        if (version_compare(PHP_VERSION, '5.5', '>=')) {
            $data['action-xmlagentxmlfile'] = new \CURLFile($xmlFile, 'application/xml', basename($xmlFile));
        } else {
            $data['action-xmlagentxmlfile'] = '@/' . realpath($xmlFile) . ';type=application/xml';
        }
        
        // Adatok elküldése és a válasz visszaadása
        return $this->send(self::HOST, 'POST', $data);
    }
    
    /**
     * 
     * @param string $xmlFile
     * @return AgentResponse
     */
    public function storno($xmlFile) {
        $data = [];
        
        if (version_compare(PHP_VERSION, '5.5', '>=')) {
            $data['action-szamla_agent_st'] = new \CURLFile($xmlFile, 'application/xml', basename($xmlFile));
        } else {
            $data['action-szamla_agent_st'] = '@/' . realpath($xmlFile) . ';type=application/xml';
        }
        
        return $this->send(self::HOST, 'POST', $data);
    }
    
    /**
     * 
     * @param string $url
     * @param string $type
     * @param array $data
     * @param array $header
     * @return AgentResponse
     */
    protected function send($url, $type = 'GET', $data = [], $header = []) {
        // Logoljuk a paramétereket
        if ($this->logger) {
            $this->logger
                    ->info('Url: ' . $url)
                    ->info('Type: ' . $type)
                    ->info('Data: ' . json_encode($data))
                    ->info('Header: ' . json_encode($header));
        }
        
        // Ha van adat és GET-el küldjük, akkor hozzá fűzzük az url-hez
        if ($data && strtoupper($type) == 'GET') {
            $data = http_build_query($data);
            if (strstr($url, '?')) {
                $url .= '&' . $data;
            } else {
                $url .= '?' . $data;
            }
        }
        
        // Logoljuk a tényleges kérés url-jét. 
        if ($this->logger) {
            $this->logger->info('Request Url: ' . $url);
        }
        
        // Létrehozzuk a süti fájlt ha nem létezne
        if ($this->cookieFile && !file_exists($this->cookieFile)) {
            file_put_contents($this->cookieFile, '');
        }
        
        // Curl létrehozása és alap beálíltások
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);

        // Süti fájl beállítása
        if ($this->cookieFile && file_exists($this->cookieFile)) {
            // sütik tárolásához
            curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile); 
            
            // Sütik megadása a kéréshez
            if (filesize($this->cookieFile) > 0) {
                curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile); 
            }
        }
        
        // Post
        if (strtoupper($type) == 'POST') {
            curl_setopt($ch,CURLOPT_POST, true);
            if ($data) {
                curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
            }
        }
        
        // Kérés indítása
        $response = curl_exec($ch);
        
        // Nincs válasz logoljuk
        if (!$response && $this->logger) {
            $this->logger->debug(curl_error($ch));
        }
        
        // Fejléc adatok lekérése
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        
        // Fejléc
        $head = substr($response, 0, $headerSize);
        
        // Válasz
        $body = substr($response, $headerSize);
        
        // HTTP Válaszkód lekérése
        $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        
        curl_close($ch);
        
        // Logoljuk a kapott adatokat
        if ($this->logger) {
            $this->logger->info('Response Header: ' . $head);
            $this->logger->info('Response Body: ' . $body);
        }
        
        // Összeállítjuk/feldolgozzuk a választ
        return new AgentResponse($httpCode, $head, $body);
    }
}