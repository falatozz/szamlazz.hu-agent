<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

require_once(__DIR__ . '/error.php');

class AgentResponse {

    /**
     *
     * @var string
     */
    protected $header;
    
    /**
     *
     * @var string
     */
    protected $body;
    
    /**
     *
     * @var int
     */
    protected $httpCode;
    
    /**
     *
     * @var Error[]
     */
    protected $errors = [];
    
    /**
     *
     * @var array
     */
    protected $data = [];
    
    /**
     * 
     * @param string $header
     * @param string $body
     */
    public function __construct($httpCode, $header, $body) {
        $this->httpCode = $httpCode;
        $this->header   = $header;
        $this->body     = $body;
        
        // Hibákhoz
        $code   = 0;
        $error  = '';
        
        foreach (explode("\n", $header) as $val) {
            $val = urldecode($val);
            
            // Számlázz.hu adatok
            if (substr($val, 0, strlen('szlahu')) === 'szlahu') {
                // A $this->data mezőben nem tárolju az "szlahu_" előtagot
                $tmp = substr($val, 7);
                $tmpPos = strpos($tmp, ': ');
                $this->data[substr($tmp, 0, $tmpPos)] = substr($tmp, $tmpPos + 2);
                
                // Hiba
                if (substr($val, 0, strlen('szlahu_error:')) === 'szlahu_error:') {
                    // sajnos volt
                    $error = substr($val, strlen('szlahu_error:'));
                }
                
                // Hiba kód
                if (substr($val, 0, strlen('szlahu_error_code:')) === 'szlahu_error_code:') {
                    // sajnos volt
                    $code = substr($val, strlen('szlahu_error_code:'));
                }
            }
        }
        
        /**
         * Server oldali hiba
         */
        if ($httpCode > 500) {
            $this->errors[] = new Error($httpCode, 0, 'Internal Server Error');
        }
        
        // Volt hiba
        if ($error || $code) {
            $this->errors[] = new Error($httpCode, $code, $error);
        }
    }
    
    /**
     * 
     * @return array
     */
    public function getHttpCode() {
        return $this->httpCode;
    }
    
    /**
     * 
     * @return array
     */
    public function getData() {
        return $this->data;
    }
    
    /**
     * 
     * @return array
     */
    public function getErrors() {
        return $this->errors;
    }
    
    /**
     * 
     * @return string
     */
    public function getHeader() {
        return $this->header;
    }
    
    /**
     * 
     * @return string
     */
    public function getBody() {
        return $this->body;
    }
    
    /**
     * 
     * @return bool
     */
    public function isPDF() {
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        return $finfo->buffer($this->body) == 'application/pdf';
    }
    
}