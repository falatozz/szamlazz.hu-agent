<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

class Error implements \JsonSerializable {
    
    /**
     *
     * @var int
     */
    protected $httpCode = 0;
    
    /**
     *
     * @var int
     */
    protected $code = 0;
    
    /**
     *
     * @var string
     */
    protected $message = '';
    
    /**
     * 
     * @param int $httpCode
     * @param int $code
     * @param string $message
     */
    public function __construct($httpCode, $code = 0, $message = '') {
        $this->httpCode = $httpCode;
        $this->code     = $code;
        $this->message  = $message;
    }
    
    /**
     * 
     * @return int
     */
    public function getHttpCode() {
        return $this->httpCode;
    }
    
    /**
     * 
     * @return int
     */
    public function getCode() {
        return $this->code;
    }
    
    /**
     * 
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }
    
    /**
     * 
     * @return array
     */
    public function jsonSerialize(): array {
        return array(
            'httpCode'  => $this->httpCode,
            'code'      => $this->code,
            'message'   => $this->message
        );
    }
}