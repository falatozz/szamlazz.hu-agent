<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Gastron.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

class ResponseData {
    
    /**
     *
     * @var string
     */
    protected $szamlaSzam       = '';
    
    /**
     *
     * @var int
     */
    protected $nettoVegosszeg   = 0;
    
    /**
     *
     * @var int
     */
    protected $bruttoVegosszeg  = 0;
    
    /**
     *
     * @var string
     */
    protected $error            = '';
    
    /**
     *
     * @var int
     */
    protected $errorCode        = 0;
    
    /**
     *
     * @var string
     */
    protected $vevoiFiokUrl     = '';
    
    /**
     * 
     * @param array $data contains: szamlaszam, nettovegosszeg, bruttovegosszeg, error, errorcode, vevoifiokurl
     */
    public function __construct($data = array()) {
        $this->szamlaSzam       = isset($data['szamlaszam']) ? $data['szamlaszam'] : '';
        $this->nettoVegosszeg   = isset($data['nettovegosszeg']) ? $data['nettovegosszeg'] : '';
        $this->bruttoVegosszeg  = isset($data['bruttovegosszeg']) ? $data['bruttovegosszeg'] : '';
        $this->error            = isset($data['error']) ? $data['error'] : '';
        $this->errorCode        = isset($data['errorcode']) ? $data['errorcode'] : '';
        $this->vevoiFiokUrl     = isset($data['vevoifiokurl']) ? $data['vevoifiokurl'] : '';
    }
    
    /**
     * 
     * @param string $szamlaSzam
     */
    public function setSzamlaSzam($szamlaSzam) {
        $this->szamlaSzam = $szamlaSzam;
    }
    
    /**
     * 
     * @param int $nettoVegosszeg
     */
    public function setNettoVegosszeg($nettoVegosszeg) {
        $this->nettoVegosszeg = $nettoVegosszeg;
    }
    
    /**
     * 
     * @param int $bruttoVegosszeg
     */
    public function setBruttoVegosszeg($bruttoVegosszeg) {
        $this->bruttoVegosszeg = $bruttoVegosszeg;
    }
    
    /**
     * 
     * @param string $error
     */
    public function setError($error) {
        $this->error = $error;
    }
    
    /**
     * 
     * @param int $errorCode
     */
    public function setErrorCode($errorCode) {
        $this->errorCode = $errorCode;
    }
    
    /**
     * 
     * @param string $vevoiFiokUrl
     */
    public function setVevoiFiokURL($vevoiFiokUrl) {
        $this->vevoiFiokUrl = $vevoiFiokUrl;
    }

    /**
     * 
     * @return string
     */
    public function getSzamlaSzam() {
        return $this->szamlaSzam;
    }
    
    /**
     * 
     * @return int
     */
    public function getNettoVegosszeg() {
        return $this->nettoVegosszeg;
    }
    
    /**
     * 
     * @return int
     */
    public function getBruttoVegosszeg() {
        return $this->bruttoVegosszeg;
    }
    
    /**
     * 
     * @return string
     */
    public function getError() {
        return $this->error;
    }
    
    /**
     * 
     * @return int
     */
    public function getErrorCode() {
        return $this->errorCode;
    }
    
    /**
     * 
     * @return string
     */
    public function getVevoiFiokURL() {
        return $this->vevoiFiokUrl;
    }
}