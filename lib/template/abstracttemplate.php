<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

abstract class AbstractTemplate {
    
    abstract public function toXML();
}