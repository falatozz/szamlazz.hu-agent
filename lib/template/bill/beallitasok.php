<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

class BillTemplateBeallitasok {
    public $felhasznalo;
    public $jelszo;
    public $eszamla = false;
    public $kulcstartojelszo;
    public $szamlaLetoltes = true;
    public $szamlaLetoltesPld = 2;
    public $valaszVerzio = 1;
    public $aggregator;
    
    public function toXML(\SimpleXMLElement &$xml) {
        $child = $xml->addChild('beallitasok');
        
        $child->addChild('felhasznalo', $this->felhasznalo);
        $child->addChild('jelszo', $this->jelszo);
        $child->addChild('eszamla', $this->eszamla ? 'true' : 'false');
        $child->addChild('kulcstartojelszo', $this->kulcstartojelszo);
        $child->addChild('szamlaLetoltes', $this->szamlaLetoltes ? 'true' : 'false');
        $child->addChild('szamlaLetoltesPld', $this->szamlaLetoltesPld);
        $child->addChild('valaszVerzio', $this->valaszVerzio);
        $child->addChild('aggregator', $this->aggregator);
    }
}