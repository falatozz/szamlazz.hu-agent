<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

require_once(__DIR__ . '/../abstracttemplate.php');
require_once(__DIR__ . '/beallitasok.php');
require_once(__DIR__ . '/elado.php');
require_once(__DIR__ . '/fejlec.php');
require_once(__DIR__ . '/tetel.php');
require_once(__DIR__ . '/tetelek.php');
require_once(__DIR__ . '/vevo.php');

class BillTemplate extends AbstractTemplate {
    
    /**
     *
     * @var BillTemplateBeallitasok
     */
    public $beallitasok;
    
    /**
     *
     * @var BillTemplateElado
     */
    public $elado;
    
    /**
     *
     * @var BillTemplateFejlec
     */
    public $fejlec;
    
    /**
     *
     * @var BillTemplateTetelek
     */
    public $tetelek;
    
    /**
     * 
     * @var BillTemplateVevo
     */
    public $vevo;
    
    /**
     * 
     */
    public function __construct() {
        $this->beallitasok  = new BillTemplateBeallitasok;
        $this->elado        = new BillTemplateElado;
        $this->fejlec       = new BillTemplateFejlec;
        $this->tetelek      = new BillTemplateTetelek;
        $this->vevo         = new BillTemplateVevo;
    }
    
    /**
     * 
     * @return String
     */
    public function toXML() {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><xmlszamla xmlns="http://www.szamlazz.hu/xmlszamla" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.szamlazz.hu/xmlszamla xmlszamla.xsd"></xmlszamla>');
        $this->beallitasok->toXML($xml);
        $this->fejlec->toXML($xml);
        $this->elado->toXML($xml);
        $this->vevo->toXML($xml);
        $this->tetelek->toXML($xml);
        
        
        $dom = dom_import_simplexml($xml)->ownerDocument;
        $dom->formatOutput = true;
        return $dom->saveXML(null, LIBXML_NOEMPTYTAG);
    }
}