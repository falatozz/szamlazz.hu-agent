<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

class BillTemplateElado {
    public $bank;
    public $bankszamlaszam;
    public $emailReplyto;
    public $emailTargy;
    public $emailSzoveg;
    
    public function toXML(\SimpleXMLElement &$xml) {
        $child = $xml->addChild('elado');
        
        $child->addChild('bank', $this->bank);
        $child->addChild('bankszamlaszam', $this->bankszamlaszam);
        $child->addChild('emailReplyto', $this->emailReplyto);
        $child->addChild('emailTargy', $this->emailTargy);
        $child->addChild('emailSzoveg', $this->emailSzoveg);
    }
    
}