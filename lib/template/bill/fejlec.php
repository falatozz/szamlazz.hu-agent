<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

class BillTemplateFejlec {
    public $keltDatum;
    public $teljesitesDatum;
    public $fizetesiHataridoDatum;
    public $fizmod = '';
    public $penznem = 'Ft';
    public $szamlaNyelve = 'hu';
    public $megjegyzes;
    public $arfolyamBank = 'MNB';
    public $arfolyam = 0.0;
    public $rendelesSzam;
    public $elolegszamla = false;
    public $vegszamla = false;
    public $dijbekero = false;
    public $szamlaszamElotag;
    public $fizetve = true;
    
    public function toXML(\SimpleXMLElement &$xml) {
        $child = $xml->addChild('fejlec');
                
        $child->addChild('keltDatum', $this->keltDatum);
        $child->addChild('teljesitesDatum', $this->teljesitesDatum);
        $child->addChild('fizetesiHataridoDatum', $this->fizetesiHataridoDatum);
        $child->addChild('fizmod', $this->fizmod);
        $child->addChild('penznem', $this->penznem);
        $child->addChild('szamlaNyelve', $this->szamlaNyelve);
        $child->addChild('megjegyzes', $this->megjegyzes);
        $child->addChild('arfolyamBank', $this->arfolyamBank);
        $child->addChild('arfolyam', $this->arfolyam);
        $child->addChild('rendelesSzam', $this->rendelesSzam);
        $child->addChild('elolegszamla', $this->elolegszamla ? 'true' : 'false');
        $child->addChild('vegszamla', $this->vegszamla ? 'true' : 'false');
        $child->addChild('dijbekero', $this->dijbekero ? 'true' : 'false');
        $child->addChild('szamlaszamElotag', $this->szamlaszamElotag);
        $child->addChild('fizetve', $this->fizetve ? 'true' : 'false');
    }
}