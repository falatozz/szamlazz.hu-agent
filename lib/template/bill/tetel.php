<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

class BillTemplateTetel {
    public $megnevezes;
    public $mennyiseg;
    public $mennyisegiEgyseg = 'db';
    public $nettoEgysegar;
    public $afakulcs;
    public $nettoErtek;
    public $afaErtek;
    public $bruttoErtek;
    public $megjegyzes;

    /**
     * 
     * @param \SimpleXMLElement $xml
     */
    public function toXML(\SimpleXMLElement &$xml) {
        $child = $xml->addChild('tetel');
                
        $child->addChild('megnevezes', $this->megnevezes);
        $child->addChild('mennyiseg', $this->mennyiseg);
        $child->addChild('mennyisegiEgyseg', $this->mennyisegiEgyseg);
        $child->addChild('nettoEgysegar', $this->nettoEgysegar);
        $child->addChild('afakulcs', $this->afakulcs);
        $child->addChild('nettoErtek', $this->nettoErtek);
        $child->addChild('afaErtek', $this->afaErtek);
        $child->addChild('bruttoErtek', $this->bruttoErtek);
        $child->addChild('megjegyzes', $this->megjegyzes);
    }
}