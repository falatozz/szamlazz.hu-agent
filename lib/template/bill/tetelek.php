<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

class BillTemplateTetelek {
    
    protected $items = array();
    
    public function add(BillTemplateTetel $item) {
        $this->items[] = $item;
    }
    
    /**
     * 
     * @return array
     */
    public function getTetelek() {
        return $this->items;
    }
    
    /**
     * 
     * @return BillTemplateTetel
     */
    public function getEmpty() {
        return new BillTemplateTetel();
    }
    
    /**
     * 
     * @return BillTemplateTetel
     */
    public function next() {
        return next($this->items);
    }
    
    /**
     * 
     * @return BillTemplateTetel
     */
    public function prev() {
        return prev($this->items);
    }
    
    /**
     * 
     * @return BillTemplateTetel
     */
    public function current() {
        return current($this->items);
    }
    
    /**
     * 
     * @return BillTemplateTetel
     */
    public function reset() {
        return reset($this->items);
    }
    
    /**
     * 
     * @param \SimpleXMLElement $xml
     */
    public function toXML(\SimpleXMLElement &$xml) {
        $child = $xml->addChild('tetelek');
                
        /* @var $item BillTemplateTetel */
        foreach ($this->items as $item) {
            $item->toXML($child);
        }
    }
}