<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Falatozz.hu
 */

namespace Falatozz\Lib\SzamlazzAgent;

class BillTemplateVevo {
    public $nev;
    public $irsz;
    public $telepules;
    public $cim;
    public $email;
    
    public function toXML(\SimpleXMLElement &$xml) {
        $child = $xml->addChild('vevo');
                
        $child->addChild('nev', $this->nev);
        $child->addChild('irsz', $this->irsz);
        $child->addChild('telepules', $this->telepules);
        $child->addChild('cim', $this->cim);
        $child->addChild('email', $this->email);
    }
}